﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NorthwindCORE.FilterModels
{
    public class UserRegisterModel
    {
        [Required(ErrorMessage = "Username is required")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Passwords are different")]
        public string ConfirmPassword { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "First Name is required")]

        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Country { get; set; }
        [Required(ErrorMessage = "Phone Number is required")]

        [MaxLength(20)]
        public int PhoneNumber { get; set; }
        [Required(ErrorMessage = "Mail is required")]

        public string EMail { get; set; }
    }
}
