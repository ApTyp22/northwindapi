﻿using Northwind.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthwindCORE.Models
{
   public  class User
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string  FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Country { get; set; }
        public int PhoneNumber { get; set; }
        public string EMail { get; set; }
        public Role Role { get; set; }

    }
}
