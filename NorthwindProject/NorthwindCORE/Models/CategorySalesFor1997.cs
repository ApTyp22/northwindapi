﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NorthwindProject
{
    public partial class CategorySalesFor1997
    {
        public string CategoryName { get; set; }
        public decimal? CategorySales { get; set; }
    }
}
