﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NorthwindProject
{
    public partial class ProductsAboveAveragePrice
    {
        public string ProductName { get; set; }
        public decimal? UnitPrice { get; set; }
    }
}
