﻿using Northwind.Core.CreateModels;
using Northwind.Core.FilterModels;
using NorthwindProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindCORE.Abstractions.Repositories
{
    public interface ICustomerRepository : ISqlReposytory<Customer>
    {
        IQueryable<CustomerViewModel> getCustomersByCountryAndCity();
        IQueryable<CustomerViewModel> CustomerByRegion();
        Task<CustomerViewModel> AddCustomerAsync(CreateCustomerModel createCustomer);
        Task EditCustomerAsync(int id, Customer customer);
        Task RemoveCustomer(int id);
    }
}
