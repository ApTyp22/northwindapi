﻿using Northwind.Core.FilterModels;
using NorthwindProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthwindCORE.Abstractions.Repositories
{
    public interface IOrderRepository : ISqlReposytory<Order>
    {
        IQueryable<OrderViewModel> HighFreightCharges();
        IQueryable<OrderViewModel> HighFreightChargesOfYear();
        IQueryable<OrderViewModel> HighFreightChargesBetween();
        IQueryable<OrderViewModel> HighFreightChargesLastYear();
    }
}
