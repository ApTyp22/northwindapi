﻿using Northwind.Core.FilterModels;
using NorthwindProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthwindCORE.Abstractions.Repositories
{
    public interface IProductRepository : ISqlReposytory<Product>
    {
     
         IQueryable<ProductViewModel> SortProducts();
            
         IQueryable<ProductViewModel> ProductsNeedReordering();
         
         IQueryable<ProductViewModel> ProductNeedReorderingContinueted();
    }
}
