﻿using NorthwindCORE.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NorthwindCORE.Abstractions.Repositories
{
    public interface IUserRepository : ISqlReposytory<User>
    {
    }
}
