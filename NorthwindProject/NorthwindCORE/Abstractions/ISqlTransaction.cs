﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NorthwindCORE.Abstractions
{
   public interface ISqlTransaction : IDisposable
    {
        void Commit();
        void RollBack();

    }
}
