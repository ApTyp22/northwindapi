﻿using Northwind.Core.FilterModels;
using System.Linq;

namespace NorthwindCORE.Abstractions
{
    public interface IOrderBL
    {
        IQueryable<OrderViewModel> HighFreightCharges();
        IQueryable<OrderViewModel> HighFreightChargesOfYear();
        IQueryable<OrderViewModel> HighFreightChargesBetween();
        IQueryable<OrderViewModel> HighFreightChargesLastYear();

    }
}
