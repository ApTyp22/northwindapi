﻿using Northwind.Core.FilterModels;
using NorthwindProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthwindCORE.Abstractions
{
    public interface IProductBL
    {
        IQueryable<ProductViewModel> SortProducts();
        IQueryable<ProductViewModel> ProductsNeedReordering();
        IQueryable<ProductViewModel> ProductNeedReorderingContinueted();

    }
}
