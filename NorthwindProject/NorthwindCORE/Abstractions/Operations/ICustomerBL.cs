﻿using Northwind.Core.CreateModels;
using Northwind.Core.FilterModels;
using NorthwindProject;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindCORE.Abstractions
{
    public interface ICustomerBL
    {
        IQueryable<CustomerViewModel> getCustomersByCountryAndCity();
        IQueryable<CustomerViewModel> CustomerByRegion();
        Task<CustomerViewModel> AddCustomerAsync(CreateCustomerModel createCustomer);
        Task EditCustomerAsync(int id, Customer customer);
        Task RemoveCustomer(int id);
    }
}
