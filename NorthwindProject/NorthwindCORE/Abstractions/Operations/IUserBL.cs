﻿using Microsoft.AspNetCore.Http;
using NorthwindCORE.FilterModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindCORE.Abstractions.Operations
{
    public interface IUserBL
    {
        Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext);
        Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext);
        Task LogOutAsync(HttpContext httpContext);
    }
}
