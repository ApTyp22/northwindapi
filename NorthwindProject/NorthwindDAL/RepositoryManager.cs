﻿using NorthwindCORE.Abstractions;
using NorthwindCORE.Abstractions.Repositories;
using NorthwindDAL.Repositories;
using NorthwindProject;
using System.Data;
using System.Threading.Tasks;

namespace NorthwindDAL
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly NorthwindContext _dbContext;
        public  RepositoryManager(NorthwindContext northwindContext)
        {
            _dbContext = northwindContext;
        } 
        
        private IOrderRepository _orders;
        public IOrderRepository Orders => _orders ?? (_orders = new OrderRepository(_dbContext)); 
       
        private ICustomerRepository _customers;
        public ICustomerRepository Customers => _customers ?? (_customers = new CustomerRepository(_dbContext));
       
        private IProductRepository _products;
        public IProductRepository Products => _products ?? (_products = new ProductRepository(_dbContext));
        
        private IUserRepository _users;
        public IUserRepository Users => _users ?? (_users = new UserRepository(_dbContext));

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(_dbContext, isolation);
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}
