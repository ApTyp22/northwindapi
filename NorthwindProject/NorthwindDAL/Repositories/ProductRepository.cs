﻿using Northwind.Core.FilterModels;
using NorthwindCORE.Abstractions.Repositories;
using NorthwindProject;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NorthwindDAL.Repositories
{
    public class ProductRepository : SqlRepositoryBase<Product>, IProductRepository
    {
        private readonly NorthwindContext _context;
        public ProductRepository(NorthwindContext dbContext) : base(dbContext)
        {
            _context = dbContext;
        }
        /* 20. Categories, and the total products in each category
               For this problem, we’d like to see the total number
               of products in each category. Sort the results by the
               total number of products, in descending order.*/

        public IQueryable<ProductViewModel> SortProducts()
        {
            var result = _context.Products
                                 .Join(_context.Categories,
                                 x => x.CategoryId, y => y.CategoryId,
                                 (x, y) => new
                                 { x.CategoryId, y.CategoryName, x.ProductId })
                                 .GroupBy(x => new { x.CategoryId, x.CategoryName })
                                 .Select(x => new ProductViewModel { CategoryName = x.Key.CategoryName, TotalProducts = x.Count() })
                                 .OrderByDescending(x => x.CategoryName);

            return result;
        }
        /* 22. Products that need reordering.
               What products do we have in our inventory that
               should be reordered? For now, just use the fields
               UnitsInStock and ReorderLevel, where
               UnitsInStock is less than the ReorderLevel,
               ignoring the fields UnitsOnOrder and
               Discontinued.
               Order the results by ProductID*/
        public IQueryable<ProductViewModel> ProductsNeedReordering()
        {
            var result = _context.Products
                                 .Where(x => x.UnitsInStock < x.ReorderLevel)
                                 .OrderBy(x => x.ProductId)
                                 .Select(x => new ProductViewModel
                                 {
                                     ProductName = x.ProductName,
                                     UnitsInStock = x.UnitsInStock,
                                     UnitsOnOrder = x.UnitsOnOrder,
                                     ReorderLevel = x.ReorderLevel,
                                     Discontinued = x.Discontinued
                                 });


            return result;
        }
        /*23.   Products that need reordering, continued
                Now we need to incorporate these fields—
                UnitsInStock, UnitsOnOrder, ReorderLevel,
                Discontinued—into our calculation.We’ll define
                “products that need reordering” with the following:
                UnitsInStock plus UnitsOnOrder are less
                than or equal to ReorderLevel
                The Discontinued flag is false (0).*/
        public IQueryable<ProductViewModel> ProductNeedReorderingContinueted()
        {
            var result = _context.Products
                                 .Where(x => x.UnitsInStock + x.UnitsOnOrder <= x.ReorderLevel && x.Discontinued == false)
                                 .OrderBy(x => x.ProductId)
                                 .Select(x => new ProductViewModel
                                 {
                                     ProductName = x.ProductName,
                                     UnitsInStock = x.UnitsInStock,
                                     UnitsOnOrder = x.UnitsOnOrder,
                                     ReorderLevel = x.ReorderLevel,
                                     Discontinued = x.Discontinued
                                 });


            return result;
        }
    }
}
