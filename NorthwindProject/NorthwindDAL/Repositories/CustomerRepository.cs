﻿using Northwind.Core.CreateModels;
using Northwind.Core.FilterModels;
using NorthwindCORE.Abstractions.Repositories;
using NorthwindCORE.Exceptions;
using NorthwindProject;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindDAL.Repositories
{
    public class CustomerRepository : SqlRepositoryBase<Customer>, ICustomerRepository
    {
        private readonly NorthwindContext _context;
        public CustomerRepository(NorthwindContext dbContext)
            : base(dbContext)
        {
            _context = dbContext;
        }
        /* 21. Total customers per country/city
               In the Customers table, show the total number of
               customers per Country and City.*/
        public IQueryable<CustomerViewModel> getCustomersByCountryAndCity()
        {
            var result = _context.Customers
                                 .GroupBy(x => new { x.Country, x.City })
                                 .Select(x => new CustomerViewModel { Country = x.Key.Country, City = x.Key.City, Count = x.Count() })
                                 .OrderByDescending(x => x.Count);

            return result;
        }
        /*   24. Customer list by region
                 A salesperson for Northwind is going on a business
                 trip to visit customers, and would like to see a list
                 of all customers, sorted by region, alphabetically.
                 However, he wants the customers with no region
                 (null in the Region field) to be at the end, instead of
                 at the top, where you’d normally find the null
                 values.Within the same region, companies should
                 be sorted by CustomerID.*/
        public IQueryable<CustomerViewModel> CustomerByRegion()
        {
            var temp = _context.Customers
                     .OrderBy(x => x.Region)
                     .ThenBy(x => x.CustomerId)
                     .Select(x => new CustomerViewModel { CompanyName = x.CompanyName, Region = x.Region })
                     .Where(x => x.Region != null || x.Region == null);


            return temp;
        }
        public async Task<CustomerViewModel> AddCustomerAsync(CreateCustomerModel createCustomer)
        {
            var customer = new Customer
            {
                CompanyName = createCustomer.CompanyName,
                Address = createCustomer.Address,
                Country = createCustomer.Country,
                Fax = createCustomer.Fax,
                PostalCode = createCustomer.PostalCode,
                Region = createCustomer.Region,
                City = createCustomer.City,
                ContactTitle = createCustomer.ContactTitle,
                ContactName = createCustomer.ContactName,
                Phone = createCustomer.Phone
            };
            var result = new CustomerViewModel
            {
                CustomerId = customer.CustomerId,
                Phone = customer.Phone,
                Address = customer.Address,
                City = customer.City,
                CompanyName = customer.CompanyName,
                Fax = customer.Fax,
                PostalCode = customer.PostalCode,
                Region = customer.Region,
                ContactName = customer.ContactName,
                ContactTitle = customer.ContactTitle,
                Country = customer.Country
            };

            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            return result;

        }
        public Task EditCustomerAsync(int id, Customer customer)
        {

            var dbCustomer = _context.Customers.Find(id);

            if (dbCustomer == null)
            {
                throw new LogicException("Wrong Customer Id");

            }
            dbCustomer.Country = customer.Country;
            dbCustomer.Address = customer.Address;
            dbCustomer.CompanyName = customer.CompanyName;
            dbCustomer.ContactName = customer.ContactName;
            dbCustomer.ContactTitle = customer.ContactTitle;
            dbCustomer.Fax = customer.Fax;
            dbCustomer.PostalCode = customer.PostalCode;
            dbCustomer.Phone = customer.Phone;
            dbCustomer.Region = customer.Region;

            _context.Customers.Update(dbCustomer);
            return _context.SaveChangesAsync();
        }
        public Task RemoveCustomer(int id)
        {
            var dbCustomer = _context.Customers.Find(id);

            if (dbCustomer == null)
            {
                throw new LogicException("Wrong Customer Id");
            }

            _context.Customers.Remove(dbCustomer);
            return _context.SaveChangesAsync();

        }


    }
}
