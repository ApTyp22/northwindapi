﻿using NorthwindCORE.Abstractions.Repositories;
using NorthwindCORE.Models;
using NorthwindDAL.Repositories;
using NorthwindProject;

namespace NorthwindDAL
{
    public class UserRepository : SqlRepositoryBase<User>, IUserRepository
    {
        public UserRepository(NorthwindContext dbContext)
            : base(dbContext)
        {

        }
    }
}
