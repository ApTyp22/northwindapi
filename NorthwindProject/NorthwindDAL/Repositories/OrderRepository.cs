﻿using Northwind.Core.FilterModels;
using NorthwindCORE.Abstractions.Repositories;
using NorthwindProject;
using System.Linq;

namespace NorthwindDAL.Repositories
{
    public class OrderRepository : SqlRepositoryBase<Order>, IOrderRepository

    {
        private readonly NorthwindContext _context;
        public OrderRepository(NorthwindContext dbContext)
          : base(dbContext)
        {
            _context = dbContext;
        }
        /* 25. High freight charges
               Some of the countries we ship to have very high
               freight charges.We'd like to investigate some more
               shipping options for our customers, to be able to
               offer them lower freight charges.Return the three
               ship countries with the highest average freight
               overall, in descending order by average freight.*/
        public IQueryable<OrderViewModel> HighFreightCharges()
        { 
            var result = _context.Orders.GroupBy(x => x.ShipCountry)
                                        .Select(x => new OrderViewModel { AverrageFreight = x.Average(x=>x.Freight) })
                                        .OrderByDescending(x => x.AverrageFreight)
                                        .Take(3);
                                        
            return result;
        }
        /*   26. High freight charges - 2015
             We're continuing on the question above on high
             freight charges.Now, instead of using all the orders
             we have, we only want to see orders from the year
             2015*/
        public IQueryable<OrderViewModel> HighFreightChargesOfYear()
        {
            var result = _context.Orders.Where(x => x.OrderDate.Value.Year == 1997)
                                        .GroupBy(x => x.ShipCountry)
                                        .Select(x => new OrderViewModel { AverrageFreight = x.Average(x => x.Freight) })
                                        .OrderByDescending(x => x.AverrageFreight)
                                        .Take(3);
                                        
            return result;
        }
        /*       27. High freight charges with between
       Another(incorrect) answer to the problem above is
       this:
           Select Top 3
            ShipCountry
            ,AverageFreight = avg(freight)
           From Orders
           Where
            OrderDate between '1/1/2015' and '12/31/2015'
           Group By ShipCountry
           Order By AverageFreight desc;
       Notice when you run this, it gives Sweden as the
       ShipCountry with the third highest freight charges.
       However, this is wrong - it should be France.
       What is the OrderID of the order that the
       (incorrect) answer above is missing?
        */
        public IQueryable<OrderViewModel> HighFreightChargesBetween()
        {

            var result = _context.Orders.Where(x => (x.OrderDate.Value.Year > 1996
                                                && x.OrderDate.Value.Year < 1997))
                                        .GroupBy(x => x.ShipCountry)
                                        .Select(x => new OrderViewModel { AverrageFreight = x.Average(x => x.Freight) })
                                        .OrderByDescending(x => x.AverrageFreight)
                                        .Take(3);
                                      
            return result;
        }
        /*28. High freight charges - last year
              We're continuing to work on high freight charges.
              We now want to get the three ship countries with
              the highest average freight charges.But instead of
              filtering for a particular year, we want to use the
              last 12 months of order data, using as the end date
              the last OrderDate in Orders.*/
        public IQueryable<OrderViewModel> HighFreightChargesLastYear()

        {
            var result = _context.Orders.Where(x=>x.OrderDate.Value.Year == 1997)
                                        .GroupBy(x => x.ShipCountry)
                                        .Select(x => new OrderViewModel
                                        {                                         
                                            Max = x.Max(x => x.Freight)                                           
                                        })                                   
                                        .OrderByDescending(x => x.Max)
                                        .Take(3);
                                        
            return result;
        }

    }
}
