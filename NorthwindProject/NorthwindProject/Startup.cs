using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NorthwindBLL.Operations;
using NorthwindCORE.Abstractions;
using NorthwindDAL;
using NorthwindCORE.Abstractions.Operations;
using Northwind.BLL.Operations;
using Microsoft.AspNetCore.Authentication.Cookies;
using NorthwindProject.Middlewares;
using Microsoft.OpenApi.Models;
using System.Threading.Tasks;

namespace NorthwindProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
       
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<NorthwindContext>(x => x.UseSqlServer(Configuration.GetConnectionString("default")));

            services.AddScoped<ICustomerBL, CustomerBL>();
            services.AddScoped<IProductBL, ProductBL>();
            services.AddScoped<IOrderBL, OrderBL>();
            services.AddScoped<IUserBL, UserBL>();
            services.AddScoped<IRepositoryManager, RepositoryManager>();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                   .AddCookie(x =>
                   {
                       x.Events.OnRedirectToLogin = (context) =>
                       {
                           context.Response.StatusCode = 401;
                           return Task.CompletedTask;
                       };
                       x.Events.OnRedirectToAccessDenied = (context) =>
                       {
                           context.Response.StatusCode = 403;
                           return Task.CompletedTask;
                       };
                   });
            services.AddSwaggerGen();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ErrorHandlingExceptions>();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
