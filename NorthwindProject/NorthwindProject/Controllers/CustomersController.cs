﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Northwind.Core.CreateModels;
using NorthwindCORE.Abstractions;
using System.Threading.Tasks;

namespace NorthwindProject.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerBL _customerBL;
        public CustomersController(ICustomerBL customerBL)
        {
            _customerBL = customerBL;
        }
        [HttpGet("Count")]
        [AllowAnonymous]
        public IActionResult getCustomersByCountryAndCity()
        {

            return Ok(_customerBL.getCustomersByCountryAndCity());
        }
        [HttpGet("Region")]
        public IActionResult CustomerByRegion()
        {
            return Ok(_customerBL.CustomerByRegion());
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddCustomerAsync([FromBody] CreateCustomerModel customerModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var addedStudent = await _customerBL.AddCustomerAsync(customerModel);

            return Created("", addedStudent);
        }
        [HttpPut("{id}")]
        public IActionResult EditCustomer([FromRoute] int id, [FromBody] Customer customer)
        {
           var result =  _customerBL.EditCustomerAsync(id,customer);

            return Ok(result);
        }
        [HttpDelete("{id}")]
        public IActionResult RemoveCustomer([FromRoute] int id)
        {
            _customerBL.RemoveCustomer(id);
            return Ok();
        }

    }
}
