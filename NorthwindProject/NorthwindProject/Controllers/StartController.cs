﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindProject.Controllers
{
        [ApiController]
        [Route("[controller]")]

    public class StartController : ControllerBase
    {
        [HttpGet]
        public IActionResult Welcome()
        {
            return Ok($"Hello Dear Client , I'm happy to see you in my first Web-Page");
        }
    }
}
