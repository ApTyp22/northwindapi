﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NorthwindCORE.Abstractions.Operations;
using NorthwindCORE.FilterModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindProject.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase

    {
        private readonly IUserBL _userBL;

        public UsersController(IUserBL userBL)
        {
            _userBL = userBL;
        }
        [HttpPost("Register")]
        public async Task<IActionResult> RegisterAsync([FromBody] UserRegisterModel registerModel)
        {
            var user = await _userBL.RegisterAsync(registerModel, HttpContext);
            return Created("", user);
        }
        [HttpPost("Login")]
        public async Task<IActionResult> LoginAsync([FromBody] UserLoginModel loginModel)
        {
            await _userBL.LoginAsync(loginModel, HttpContext);
            return Ok();
        }
        [HttpPost("Logout")]
        public async Task<IActionResult> LogOutAsync()
        {
            await _userBL.LogOutAsync(HttpContext);
            return Ok();
        }

    }
}
