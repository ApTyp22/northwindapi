﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NorthwindCORE.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindProject.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize (Roles = "User")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderBL _orderBL;

        public OrdersController(IOrderBL orderBL)
        {
            _orderBL = orderBL;
        }
        [HttpGet("HighFreight")]
        public IActionResult HighFreightCharges()
        {
            return Ok(_orderBL.HighFreightCharges());
        }
        [HttpGet("HighFreightCharges")]
        public IActionResult HighFreightChargesOfYear()
        {
            return Ok(_orderBL.HighFreightChargesOfYear());
        }
        [HttpGet("HighFreightBetween")]
        public IActionResult HighFreightChargesBetween()
        {
            return Ok(_orderBL.HighFreightChargesBetween());
        }
        [HttpGet("HighFreightLastYear")]
        public IActionResult HighFreightChargesLastYear()
        {
            return Ok(HighFreightChargesLastYear());
        }
    }
}
