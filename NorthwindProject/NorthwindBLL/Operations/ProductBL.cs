﻿using Microsoft.Extensions.Logging;
using Northwind.Core.FilterModels;
using NorthwindCORE.Abstractions;
using NorthwindCORE.Abstractions.Repositories;
using NorthwindProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthwindBLL.Operations
{
    public class ProductBL : IProductBL
    {
        private readonly IRepositoryManager _repositoryManager;
        private ILogger<ProductBL> _logger;
        public ProductBL(IRepositoryManager repositoryManager,ILogger<ProductBL> logger)
        {
            _logger = logger;
            _repositoryManager = repositoryManager;
        }
        public IQueryable<ProductViewModel> SortProducts()
        {
            _logger.LogInformation($"This method has started");
            var SortedProducts = _repositoryManager.Products.SortProducts();                
             return SortedProducts;
            _logger.LogInformation("This method was finished");
         }
        public IQueryable<ProductViewModel> ProductsNeedReordering()
        {
            _logger.LogInformation($"This method has started");
            return _repositoryManager.Products.ProductsNeedReordering();
            _logger.LogInformation("This method was finished");
        }
        public IQueryable<ProductViewModel> ProductNeedReorderingContinueted()
        {
            _logger.LogInformation($"This method has started");
            return _repositoryManager.Products.ProductNeedReorderingContinueted();
            _logger.LogInformation("This method was finished");
        }
        
    }
}
