﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NorthwindCORE.Abstractions;
using NorthwindCORE.Abstractions.Operations;
using NorthwindCORE.Exceptions;
using NorthwindCORE.FilterModels;
using NorthwindCORE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.BLL.Operations
{
    public class UserBL : IUserBL
    {
        private readonly IRepositoryManager _repositories;
        private ILogger<UserBL> _logger;

        public UserBL(IRepositoryManager repositories,ILogger<UserBL> logger)
        {
            _logger = logger;
            _repositories = repositories;
        }
        private async Task Authenticate(User user, HttpContext httpContext)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim(ClaimsIdentity.DefaultRoleClaimType,user.Role.ToString()),
                new Claim("ID",user.ID.ToString())
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
        public async Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext)
        {
            _logger.LogInformation("Someone want to Log in");
            var users = _repositories.Users.GetWhere(x => x.UserName == loginModel.UserName && x.Password == loginModel.Password);
            
            if (users == null || !users.Any())
            {
                throw new LogicException("Wrong username or/and password");
            }

            await Authenticate(users.First(), httpContext);
            _logger.LogInformation($"Log in succesful on {DateTime.Now}");
            
        }

        public async Task LogOutAsync(HttpContext httpContext)
        {
            
            await httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            _logger.LogInformation($"This Accaunt logged out , {DateTime.Now}");

        }

        public async Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext)
        {
            _logger.LogInformation($"Register just started");
            var users = _repositories.Users.GetWhere(x => x.UserName == registerModel.UserName);

            if (users.Any())
            {
                throw new LogicException("Username is already taken");
            }
            var user = new User
            {
                UserName = registerModel.UserName,
                Password = registerModel.Password,
                FirstName = registerModel.FirstName,
                LastName = registerModel.LastName,
                Country = registerModel.Country,
                BirthDate = registerModel.BirthDate,
                EMail = registerModel.EMail,
                PhoneNumber = registerModel.PhoneNumber
            };
            _repositories.Users.Add(user);

            await _repositories.SaveChangesAsync();

            await Authenticate(user, httpContext);

            return new UserViewModel
            {
                ID = user.ID,
                FirstName = user.FirstName,
                Country = user.Country,
                EMail = user.EMail,
                LastName = user.LastName
            };
            _logger.LogInformation($"Register is succesful,new User added to database {DateTime.Now}");

        }
    }
}
