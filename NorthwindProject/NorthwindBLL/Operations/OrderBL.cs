﻿using Microsoft.Extensions.Logging;
using Northwind.Core.FilterModels;
using NorthwindCORE.Abstractions;
using NorthwindCORE.Abstractions.Repositories;
using NorthwindProject;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthwindBLL.Operations
{
    public class OrderBL : IOrderBL
    {

        private readonly IRepositoryManager _repositoryManager;
        private ILogger<OrderBL> _logger;
        public OrderBL(IRepositoryManager repositoryManager, ILogger<OrderBL> logger)
        {
            _logger = logger;
            _repositoryManager = repositoryManager;
        }
        public IQueryable<OrderViewModel> HighFreightCharges()
        {
            _logger.LogInformation($"This method has started");
            return _repositoryManager.Orders.HighFreightCharges();
            _logger.LogInformation("This method was finished");
        }

        public IQueryable<OrderViewModel> HighFreightChargesBetween()
        {
            _logger.LogInformation($"This method has started");
            return _repositoryManager.Orders.HighFreightChargesBetween();
            _logger.LogInformation("This method was finished");
        }

        public IQueryable<OrderViewModel> HighFreightChargesOfYear()
        {
            _logger.LogInformation($"This method has started");
            return _repositoryManager.Orders.HighFreightChargesOfYear();
            _logger.LogInformation("This method was finished");
        }
        public IQueryable<OrderViewModel> HighFreightChargesLastYear()
        {
            _logger.LogInformation($"This method has started");
            return _repositoryManager.Orders.HighFreightChargesLastYear();
            _logger.LogInformation("This method was finished");
        }
        /*public Array sortOrdersByEmpAndProduct(Order orderModel)
        {
            var sortOrdersbyEmployeeAndProduct = _northwindContext.Orders.Join(_northwindContext.Employees, x => x.EmployeeId, y => y.EmployeeId, (x, y) => new { y.EmployeeId, y.LastName, x.OrderId })
                                                            .Join(_northwindContext.OrderDetails, x => x.OrderId, y => y.OrderId, (x, y) => new { x.EmployeeId, x.LastName, x.OrderId, y.ProductId, y.Quantity })
                                                            .Join(_northwindContext.Products, x => x.ProductId, y => y.ProductId, (x, y) => new { x.EmployeeId, x.LastName, x.OrderId, x.ProductId, y.ProductName, x.Quantity })
                                                            .OrderBy(x => x.OrderId)
                                                            .ThenBy(y => y.ProductId)
                                                            .Select(x => new { x.EmployeeId, x.LastName, x.OrderId, x.ProductName, x.Quantity })
                                                            .ToArray();

            return sortOrdersbyEmployeeAndProduct;
        }*/
    }
}
