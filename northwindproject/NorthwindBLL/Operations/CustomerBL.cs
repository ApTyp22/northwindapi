﻿using Microsoft.Extensions.Logging;
using Northwind.Core.CreateModels;
using Northwind.Core.FilterModels;
using NorthwindCORE.Abstractions;
using NorthwindCORE.Exceptions;
using NorthwindProject;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindBLL.Operations
{
    public class CustomerBL : ICustomerBL
    {

        private readonly IRepositoryManager _repositoryManager;
        private readonly ILogger<CustomerBL> _logger;
        public CustomerBL(IRepositoryManager repositoryManager, ILogger<CustomerBL> logger)
        {
            _logger = logger;
            _repositoryManager = repositoryManager;
        }
        public IQueryable<CustomerViewModel> getCustomersByCountryAndCity()
        {
            _logger.LogInformation($"This method has started");
            var GetCustomer = _repositoryManager.Customers.getCustomersByCountryAndCity();
            if (GetCustomer == null)
            {
                throw new LogicException("Something went Wrong");
            }
            return GetCustomer;
            _logger.LogInformation("This method was finished");

        }
        public IQueryable<CustomerViewModel> CustomerByRegion()
        {
            _logger.LogInformation($"This method has started");

            var result = _repositoryManager.Customers.CustomerByRegion();
            if (result.Equals(null))
            {
                throw new LogicException("Something went wrong");
            }
            return result;
            _logger.LogInformation("This method was finished");

        }
        public async Task<CustomerViewModel> AddCustomerAsync(CreateCustomerModel createCustomer)
        {

            _logger.LogInformation($"This method has started");
            var result = await _repositoryManager.Customers.AddCustomerAsync(createCustomer);
            if (result.Equals(null))
            {
                throw new LogicException("Something went wrong");
            }
            return result;
            _logger.LogInformation("This method was finished");

        }
        public Task EditCustomerAsync(int id, Customer customer)
        {
            var result = _repositoryManager.Customers.EditCustomerAsync(id, customer);
            if (result.Equals(null))
            {
                throw new LogicException("Something went wrong");
            }
            return result;
            
        }

        public Task RemoveCustomer(int id)
        {
            var result = _repositoryManager.Customers.RemoveCustomer(id);
            if (result.Equals(null))
            {
                throw new LogicException("Something went wrong");
            }
            return result;
        }
    }
}
